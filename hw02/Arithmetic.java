// Olivia Braunstein
// 9/6/18

public class Arithmetic{
  // main method required for every java program
  public static void main(String args[]) {
    
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;
 
//the pa tax rate
double paSalesTax = 0.06;

// my output values
double totalCostPants; // total cost of pants
double totalCostShirts; // total cost of shirts
double totalCostBelts; // total cost of belts
double costPantsTax; // sales tax on pants
double costShirtsTax; // sales tax on shirts
double costBeltsTax; // sales tax on belts
double totalCostOfPurchase; // total cost without tax
double totalSalesTax; // total sales tax on all items
double totalAmountPaidWithTax; // total amount paid including tax

    
// calculations for costs without tax
totalCostPants = numPants * pantsPrice;
totalCostShirts = numShirts * shirtPrice;
totalCostBelts = numBelts * beltPrice;

// calculations for pa sales tax
costPantsTax = totalCostPants * paSalesTax;
costShirtsTax = totalCostShirts * paSalesTax;
costBeltsTax = totalCostBelts * paSalesTax;
    
// cost for shirts pants and belt
totalCostOfPurchase = totalCostPants + totalCostShirts + totalCostBelts;

// sales tax on shirts pants and belts
totalSalesTax = totalCostPants * paSalesTax + totalCostShirts * paSalesTax + totalCostBelts * paSalesTax;

// final cost including sales tax 
totalAmountPaidWithTax = totalCostOfPurchase + totalSalesTax;
    
// to ensure proper decimals
costPantsTax = costPantsTax * 100;
costPantsTax = (int)costPantsTax;
costPantsTax = costPantsTax / 100;
    
costShirtsTax = costShirtsTax * 100;
costShirtsTax = (int)costShirtsTax;
costShirtsTax = costShirtsTax / 100;
  
costBeltsTax = costBeltsTax * 100;
costBeltsTax = (int)costBeltsTax;
costBeltsTax = costBeltsTax / 100;
    
totalSalesTax = totalSalesTax * 100;
totalSalesTax = (int)totalSalesTax;
totalSalesTax = totalSalesTax / 100;
 
totalAmountPaidWithTax = totalAmountPaidWithTax * 100;
totalAmountPaidWithTax = (int)totalAmountPaidWithTax;
totalAmountPaidWithTax = totalAmountPaidWithTax / 100;

// Print all of the values
System.out.println("The total cost of pants before tax is " + totalCostPants + " dollars");
System.out.println("The total cost of shirts before tax is " + totalCostShirts + " dollars");
System.out.println("The total cost of belts before tax is " + totalCostBelts + " dollars");
    
System.out.println("The total amount of tax on pants is " + costPantsTax + " dollars");
System.out.println("The total amount of tax on shirts is " + costShirtsTax + " dollars");
System.out.println("The total amount of tax on belts is " + costBeltsTax + " dollars");
    
System.out.println("The total cost of purchase before tax is " + totalCostOfPurchase + " dollars");
System.out.println("The total amount of sales tax on the purchase is " + totalSalesTax + " dollars");
System.out.println("The total amount paid with tax is " + totalAmountPaidWithTax + " dollars");
}
}








