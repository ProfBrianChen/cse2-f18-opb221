// Olivia Braunstein
// 9/16/18

import java.util.Scanner; // introduce the scanner
public class Convert{
  // main method required for every java program
  public static void main(String args[]) {
   
    Scanner myScanner = new Scanner(System.in);
  System.out.print("Enter the affected area in acres: "); // input the affected area in acres
    double acresArea = myScanner.nextDouble(); 
  System.out.print("Enter the rainfall in the affected area: "); // another input for the rainfall in affected area
    double amountRain = myScanner.nextDouble(); 
   
  amountRain = acresArea * amountRain; // mutiply area by rainfall amount
  amountRain = amountRain * 27154; // multiply by the amount in gallons
  amountRain = amountRain * 9.08E-13; // another multiplication to get cubic miles
  System.out.println( amountRain + " cubic miles");
  }
}

  