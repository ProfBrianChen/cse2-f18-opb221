// Olivia Braunstein
// 9/17/18

import java.util.Scanner; // introduce scanner

public class Pyramid{
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("The square side of the pyramid is (input length): "); // input value for the length of one side of the square base
    int squareSide = myScanner.nextInt();
    System.out.print("The height of the pyramid is: "); // input value for the height of the pyramid
    int height = myScanner.nextInt();
    
   int v1 = squareSide * squareSide; // find a^2 with multiplication
   int v2 = height * v1; // multiply the height
   int v3 = v2/3; // divide by 3
    
    
    
    System.out.println("your volume is:" + v3);

  }
}