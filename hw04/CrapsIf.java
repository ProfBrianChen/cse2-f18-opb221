// Olivia Brauntein
// 9/21/18 - hw 4
// Using if statements ONLY
// Explanation: First choose to either randomly generate two numbers between 1 and 6 inclusive 
// or cast two numbers manually.
// Then based on the selection, print the proper slang terminaology for the two numbers

import java.lang.Math; // import math
import java.util.Scanner; // import scanner
    public class CrapsIf {
    public static void main (String[] args){ // main method used in all scripts
        Scanner myScanner = new Scanner(System.in); // introduce the scanner to input numbers
        System.out.print("If you would like to cast two dice randomly, type '1', if you would like to choose, type '2' "); // choose to cast a number or randomly generate one
        int diceChoice = myScanner.nextInt();
        
        int valueOne; // initialize values 1 and 2 for rolls
        int valueTwo;
     
        if (diceChoice == 2){
          System.out.print("Type the first value (between 1 and 6 inclusive): "); // user inputs first number when rolling
             valueOne = myScanner.nextInt();
          System.out.print("Type the second value (between 1 and 6 inclusive): "); // user inputs the second number after rolling
             valueTwo = myScanner.nextInt();
          
     
        
        if (valueOne == 1 && valueTwo == 1) { // use the values entered to find the slang term for this roll
      System.out.println("This is Snake Eyes");
       }
        else if (valueOne == 2 && valueTwo == 1 || valueOne == 1 && valueTwo == 2) { // use the same steps as line 23, print the slang term, in this case "ace deuce"
      System.out.println("This is Ace Deuce");
        }  
        else if (valueOne == 3 && valueTwo == 1 || valueOne == 1 && valueTwo == 3) { // use same steps as line 23, up to line 91
      System.out.println("This is Easy Four"); 
        }    
         else if (valueOne == 4 && valueTwo == 1 || valueOne == 1 && valueTwo == 4) {
      System.out.println("This is Fever Five");
         }     
         else if (valueOne == 5 && valueTwo == 1 || valueOne == 1 && valueTwo == 5) {
      System.out.println("This is Easy Six");
         }        
         else if (valueOne == 6 && valueTwo == 1 || valueOne == 1 && valueTwo == 6) {
      System.out.println("This is Seven Out");
         }          
     
         else if (valueOne == 2 && valueTwo == 2) { // only 2 and 2 because they are the same value so no need to reverse the numbers
      System.out.println("This is Hard Four");
         }   
         else if (valueOne == 2 && valueTwo == 3 || valueOne == 3 && valueTwo == 2) {
      System.out.println("This is Fever Five");
         }  
         else if (valueOne == 2 && valueTwo == 4 || valueOne == 4 && valueTwo == 2) {
      System.out.println("This is Easy Six");
         } 
        else if (valueOne == 2 && valueTwo == 5 || valueOne == 5 && valueTwo == 2) {
      System.out.println("This is Seven Out"); 
        }    
         else if (valueOne == 2 && valueTwo == 6 || valueOne == 6 && valueTwo == 2) {
      System.out.println("This is Easy Eight");
         }      
        
           
         else if (valueOne == 3 && valueTwo == 3) {
      System.out.println("This is Hard Six");
         }       
         else if (valueOne == 3 && valueTwo == 4 || valueOne == 4 && valueTwo == 3) {
      System.out.println("This is Seven Out");
         } 
         else if (valueOne == 3 && valueTwo == 5 || valueOne == 5 && valueTwo == 3) {
      System.out.println("This is Easy Eight");
         }
          else if (valueOne == 3 && valueTwo == 6 || valueOne == 6 && valueTwo == 3) {
      System.out.println("This is Nine");
          }  
           
             
          else if (valueOne == 4 && valueTwo == 4) {
      System.out.println("This is Easy Ten");
          }      
         else if (valueOne == 4 && valueTwo == 5 || valueOne == 5 && valueTwo == 4) {
      System.out.println("This is Nine");
         }
         else if (valueOne == 4 && valueTwo == 6 || valueOne == 6 && valueTwo == 4) {
      System.out.println("This is Easy Ten");
         } 
            
            
          else if (valueOne == 5 && valueTwo == 5) {
      System.out.println("This is Hard Ten");
          }      
         else if (valueOne == 5 && valueTwo == 6 || valueOne == 6 && valueTwo == 5) {
      System.out.println("This is Yo-leven");
         }
         else if (valueOne == 6 && valueTwo == 6) {
      System.out.println("This is Boxcars");
         
         }
           
      else if  (valueOne < 1 || valueOne > 6 || valueTwo < 1 || valueTwo > 6) {
      System.out.println("These numbers are not valid");
      }
        }  
      else if (diceChoice == 1){ // takes into account the option to generate randomly
             valueOne = (int)(Math.random()*(6)) + 1; // equation to generate a random number
             valueTwo = (int)(Math.random()*(6)) + 1;
  
             
      if (valueOne == 1 && valueTwo == 1) { // same process as lines 23-91
      System.out.println("This is Snake Eyes"); // prints the slang terminology based on values one and two
       }
      else if (valueOne == 2 && valueTwo == 1 || valueOne == 1 && valueTwo == 2) {
      System.out.println("This is Ace Deuce");
        }  
      else if (valueOne == 3 && valueTwo == 1 || valueOne == 1 && valueTwo == 3) {
      System.out.println("This is Easy Four"); 
        }    
      else if (valueOne == 4 && valueTwo == 1 || valueOne == 1 && valueTwo == 4) {
      System.out.println("This is Fever Five");
         }     
      else if (valueOne == 5 && valueTwo == 1 || valueOne == 1 && valueTwo == 5) {
      System.out.println("This is Easy Six");
         }        
      else if (valueOne == 6 && valueTwo == 1 || valueOne == 1 && valueTwo == 6) {
      System.out.println("This is Seven Out");
         }          
     
   else if (valueOne == 2 && valueTwo == 2) {
   System.out.println("This is Hard Four");
         }    
   else if (valueOne == 2 && valueTwo == 3 || valueOne == 3 && valueTwo == 2) {
   System.out.println("This is Fever Five");
         }  
   else if (valueOne == 2 && valueTwo == 4 || valueOne == 4 && valueTwo == 2) {
   System.out.println("This is Easy Six");
         } 
   else if (valueOne == 2 && valueTwo == 5 || valueOne == 5 && valueTwo == 2) {
   System.out.println("This is Seven Out"); 
        }    
   else if (valueOne == 2 && valueTwo == 6 || valueOne == 6 && valueTwo == 2) {
   System.out.println("This is Easy Eight");
         }      
        
           
   else if (valueOne == 3 && valueTwo == 3) {
   System.out.println("This is Hard Six");
         }       
   else if (valueOne == 3 && valueTwo == 4 || valueOne == 4 && valueTwo == 3) {
   System.out.println("This is Seven Out");
         } 
   else if (valueOne == 3 && valueTwo == 5 || valueOne == 5 && valueTwo == 3) {
   System.out.println("This is Easy Eight");
         }
   else if (valueOne == 3 && valueTwo == 6 || valueOne == 6 && valueTwo == 3) {
   System.out.println("This is Nine");
          }  
           
             
   else if (valueOne == 4 && valueTwo == 4) {
   System.out.println("This is Easy Ten");
          }      
   else if (valueOne == 4 && valueTwo == 5 || valueOne == 5 && valueTwo == 4) {
   System.out.println("This is Nine");
         }
   else if (valueOne == 4 && valueTwo == 6 || valueOne == 6 && valueTwo == 4) {
   System.out.println("This is Easy Ten");
         } 
            
            
  else if (valueOne == 5 && valueTwo == 5) {
  System.out.println("This is Hard Ten");
          }      
  else if (valueOne == 5 && valueTwo == 6 || valueOne == 6 && valueTwo == 5) {
  System.out.println("This is Yo-leven");
         }
  else if (valueOne == 6 && valueTwo == 6) {
  System.out.println("This is Boxcars");
  }
}
}
}

    
    