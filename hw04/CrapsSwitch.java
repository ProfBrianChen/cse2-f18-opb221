// Olivia Brauntein
// 9/21/18 - hw 4
// Using switch statements ONLY
// Explanation: First choose to either randomly generate two numbers between 1 and 6 inclusive 
// or cast two numbers manually.
// Then based on the selection, print the proper slang terminaology for the two numbers

 import java.lang.Math; // import math
 import java.util.Scanner; // import scanner
 import java.util.Random;

 public class CrapsSwitch {
 public static void main (String[] args){ // main method used in all scripts
 Scanner myScanner = new Scanner(System.in); // introduce the scanner to input numbers
 Random ranGen = new Random();
   
 int valueOne; // initialize values 1 and 2 for rolls
 int valueTwo;  
  
 System.out.print("If you would like to cast two dice randomly, type '1', if you would like to choose, type '2' "); // choose to cast a number or randomly generate one
 int diceChoice = myScanner.nextInt();
 valueOne = (int)(Math.random()*(6)) + 1; // equation to generate a random number
 valueTwo = (int)(Math.random()*(6)) + 1; // equation to generate a random number  
  
 
   
 int diceSum = (valueOne + valueTwo); // use a sum to print with minimal lines
   
  switch (diceChoice){ // switch based on the choice between input and random
    case 1:  // case 1 is random
  if (valueOne == valueTwo) {
  switch (diceSum){  // to dictate when its a hard number
     case 4:
      valueOne = valueTwo;
       System.out.println("This slang term is Hard Four");
     break;
     case 6:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Six");
     break;
     case 8:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Eight");
     break;
     case 10:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Ten");
     break;
   }
  }
     
   switch (diceSum){ 
    case 2: 
      System.out.println("This slang term is Snake Eyes");
    break;
      
    case 3: 
      System.out.println("This slang term is Ace Deuce");
    break;
       
    case 4: 
      System.out.println("This slang term is Hard Four");
    break;
      
    case 5: 
      System.out.println("This slang term is Fever Five");
    break;
       
    case 6: 
      System.out.println("This slang term is Easy Six");
    break;
      
    case 7: 
      System.out.println("This slang term is Seven Out");
    break;
       
    case 8: 
      System.out.println("This slang term is Easy Eight");
    break;
      
    case 9: 
      System.out.println("This slang term is Nine");
    break;
       
    case 10: 
      System.out.println("This slang term is Easy Ten");
    break;
      
    case 11: 
      System.out.println("This slang term is Yoleven");
    break;
       
    case 12: 
      System.out.println("This slang term is Boxcars");
    break;
       
   }
  }
   switch (diceChoice) {
    case 2:
     System.out.print("Type the first value (between 1 and 6 inclusive): "); // user inputs first number when rolling
      valueOne = myScanner.nextInt();
     System.out.print("Type the second value (between 1 and 6 inclusive): "); // user inputs the second number after rolling
      valueTwo = myScanner.nextInt();    
     
       diceSum = valueOne + valueTwo;
   
    if (valueOne == valueTwo) {
    switch(diceSum){   
   
     case 4:
      valueOne = 2;
      valueTwo = 2;
       System.out.println("This slang term is Hard Four");
     break;
     case 6:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Six");
     break;
     case 8:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Eight");
     break;
     case 10:
       valueOne = valueTwo;
       System.out.println("This slang term is Hard Ten");
     break;
   } 
    }
      
   switch (diceSum){ 
   
    case 2: 
      System.out.println("This slang term is Snake Eyes");
    break;
      
    case 3: 
      System.out.println("This slang term is Ace Deuce");
    break;
       
    case 4:
      System.out.println("This slang term is Easy Four");
    break;
      
    case 5: 
      System.out.println("This slang term is Fever Five");
    break;
       
    case 6: 
      System.out.println("This slang term is Easy Six");
    break;
      
    case 7: 
      System.out.println("This slang term is Seven Out");
    break;
       
    case 8: 
      System.out.println("This slang term is Easy Eight");
    break;
      
    case 9: 
      System.out.println("This slang term is Nine");
    break;
       
    case 10: 
      System.out.println("This slang term is Easy Ten");
    break;
      
    case 11: 
      System.out.println("This slang term is Yoleven");
    break;
       
    case 12: 
      System.out.println("This slang term is Boxcars");
    break;
    
          
      }
      }
  }
 }
 
 

 


 

    
    
     