// Olivia Braunstein
// 9/6/18

public class Cyclometer{
  // main method required for every java program
  public static void main(String args[]) {
    
    int secsTrip1 = 480; // initialize the seconds for trip 1
    int secsTrip2 = 3220; // initialize the seconds for trip 2
    int countsTrip1 = 1561; // initialize number of rotations for trip 1
    int countsTrip2 = 9037; // initialize number of rotations for trip 2
    
    double wheelDiameter = 27.0, // diameter of the wheel
    PI = 3.14159, // value for Pi
    feetPerMile = 5280, // number of feet in a  mile
    inchesPerFoot = 12, // number of inches in a foot
    secondsPerMinute = 60; // number of seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; // variables for distances
    
    // Print the stored numbers
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
   
    // Compute the values for the distances
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; // Gives distance of trip 1 in miles
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance of trip 1 in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // Gives distance of trip 2 in miles
	  totalDistance = distanceTrip1 + distanceTrip2; // Gives total distance
    
    // Print the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // Trip 1 Distance
    System.out.println("Trip 2 was " + distanceTrip2 + " miles"); // Trip 2 Distance
    System.out.println("The total distance was " + totalDistance + " miles"); // Total Distance
  }
}
