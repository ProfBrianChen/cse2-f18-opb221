// Olivia Brauntein
// 9/13/19 - lab 3
// Obtain the original cost of the check from the user
// find the percentage tip they wish to pay
// And the number of ways to be split
// Finally discover how much each person needs to pay in order the pay the full check

import java.util.Scanner;
  public class Check{
      public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in);
              System.out.print("Enter the original cost of the check in the form xx.xx:"); // user inputs cost
        double checkCost = myScanner.nextDouble(); // declare double "checkCost", total cost
              System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx)"); // user inputs amount of tip they wish to pay
        double tipPercent = myScanner.nextDouble(); // declare double " tipPercent", amoutn of tip user input above
              tipPercent /= 100; // convert the percentage into a decimal value
              System.out.print("Enter the number of people who went out to dinner: "); // user inputs number of people at dinner
        int numPeople = myScanner.nextInt(); // declare an int for the number of people at dinner
        double totalCost; // declare double for the total cost
        double costPerPerson; // declare a double for cost per person
        int dollars, dimes, pennies; // declare the interger dollars, whole dollar amount of cost, dimes and pennies
            totalCost = checkCost * (1 + tipPercent); // solve for total
            costPerPerson = totalCost / numPeople; // divide total by numebr of people for individual checks
            dollars = (int)costPerPerson; // amount of whole dollars
            dimes = (int)(costPerPerson * 10) % 10; // amount of dimes
            pennies = (int)(costPerPerson * 100) % 10; // amoutn of pennies
            System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); // print the amount owed by each person
        
  }
}