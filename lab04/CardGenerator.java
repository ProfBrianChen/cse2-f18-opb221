// Olivia Brauntein
// 9/20/19 - lab 4
// Write a program that will pick a random card from the deck
// Random number generator to select a number 1-52
// 1-13 are diamonds, 14-26 are clubs, and 27-39 are hearts, 40-52 are spades
import java.lang.Math; // import math
  public class CardGenerator{
      public static void main (String[] args){
      int cardNumber = (int)(Math.random()*(51)) + 1;
        
        
         if (1 <= cardNumber && cardNumber <= 13) { // setting the conditions for the if statement (diamonds)
          
           if (cardNumber == 1){ // Printing the word ace rather than 1
           System.out.println("This is an ace of diamonds.");
          }
            else if (cardNumber == 11){ // Printing the word jack rather than 11
            System.out.println("This is a jack of diamonds.");
           
         }
           else if (cardNumber == 12){ // Printing the word queen rather than 12
              System.out.println("This is a queen of diamonds.");
          }
           else if (cardNumber == 13){ // Printing the word king rather than 13
            System.out.println("This is a king of diamonds.");
          }
           else  // use else to print all cards between 1 and 10 rather than ace, queen, king, or jack
            System.out.println("This is a " + (cardNumber % 13) + " of diamonds."); // use modulo to avoid printing card numbers greater than 10
         }
        
       
        
         if (14 <= cardNumber && cardNumber <= 26){ // same steps for the clubs (14-26)
        
           if (cardNumber == 24){
              cardNumber = cardNumber % 13;
              System.out.println("This is a Jack of clubs.");
           } 
            else if (cardNumber == 25){
            cardNumber = cardNumber % 13;
            System.out.println("This is a queen of clubs.");
            }   
            else if (cardNumber == 26){
            cardNumber = cardNumber % 13;
              System.out.println("This is a king of clubs.");
          }
              else if (cardNumber == 14){
            cardNumber = cardNumber % 13;
              System.out.println("This is an ace of clubs.");
          }
           else
             System.out.println("This is a " + (cardNumber % 13) + " of clubs.");
         }
          
        

         if (27 <= cardNumber && cardNumber <= 39) { // same steps for the hearts (27-39)
 
           if (cardNumber == 37){
            cardNumber = cardNumber % 13;
              System.out.println("This is a Jack of hearts.");
         }
           else if (cardNumber == 27){
            cardNumber = cardNumber % 13;
              System.out.println("This is an ace of hearts.");
          }
            else if (cardNumber == 38){
            cardNumber = cardNumber % 13;
              System.out.println("This is a queen of hearts.");
          }
           else if (cardNumber == 39){
            cardNumber = cardNumber % 13;
              System.out.println("This is a king of hearts.");
          }
           else
              System.out.println("This is a " + (cardNumber % 13) + " of hearts."); 
         }
         
           
           
           
        if (40 <= cardNumber && cardNumber <= 52){ // same steps for the spades (40-52)
        
          if (cardNumber == 50){
            cardNumber = cardNumber % 13;
              System.out.println("This is a Jack of spades.");
         }
          else if (cardNumber == 40){
            cardNumber = cardNumber % 13;
              System.out.println("This is an ace of spades.");
          }
          else if (cardNumber == 51){
            cardNumber = cardNumber % 13;
              System.out.println("This is a queen of spades.");
          }
          
          else if (cardNumber == 52){
            cardNumber = cardNumber % 13;
              System.out.println("This is a king of spades.");
          }
          else
              System.out.println("This is a " + (cardNumber% 13) + " of spades.");
        }    
         }
  
          
        }
                   
                             