// Olivia Braunstein
// 10/4 - Lab 5
// Write a script that asks a student about any class they are currently in, checking the input is of the right type
import java.util.Scanner; // import scanner
  public class UserInput{
      public static void main (String[] args){ // main method used in every script
        Scanner myScanner = new Scanner(System.in);
        
          System.out.println("Input your course number: "); // first input
          boolean course = myScanner.hasNextInt(); // use a boolean to check if it is an int
        
        while (course == false){ // while state to correct the type
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out input with the wrong type
          System.out.println("Input your course number: ");
          course = myScanner.hasNextInt();
        }
        if(course){
          int courseNumber = myScanner.nextInt(); // store proper variable type
        }
        
        
        
          System.out.println("Input the department name: ");
          boolean name = myScanner.hasNextInt(); // use a boolean to check if it is an int
        
        while (name == true){ // if int, it is false (should be a name) 
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out input with the wrong type
          System.out.println("Input the department name: ");
          name = myScanner.hasNextInt();
        }
        if(name == false){
          String departmentName = myScanner.next(); // store proper variable type
        }

       
        
        System.out.println("Input number of times it meets in a week: ");
          boolean meets = myScanner.hasNextInt(); // use a boolean to check if it is an int
        
        while (meets == false){
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out input with the wrong type
          System.out.println("Input number of times it meets in a week: ");
          meets = myScanner.hasNextInt();
        }
        if(meets){
          int meetTimes = myScanner.nextInt(); // store proper variable type
        }
        
        
        
        System.out.println("Input the time class starts (10:30 = 10.30): ");
          boolean time = myScanner.hasNextDouble(); // use a boolean to check if it is a double
        
        while (time == false){
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out input with the wrong type
          System.out.println("Input the time class starts: ");
          time = myScanner.hasNextDouble();
        }
        if(time){
          double startTime = myScanner.nextDouble(); // store proper variable type
        }
        
        
        
        System.out.println("Input the instructor's name: ");
          boolean prof = myScanner.hasNextInt(); // use a boolean to check if it is an int
        
        while (prof == true){ // if int, it is false (should be a name)
          System.out.println("Wrong input type. Please try again.");
          String junk = myScanner.next(); // throw out input with the wrong type
          System.out.println("Input the instructor's name: ");
          prof = myScanner.hasNextInt();
        }
        if(prof == false){
          String professorName = myScanner.next(); // store proper variable type
        }
        
      }
  }